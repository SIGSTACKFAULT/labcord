<?php

$HOOKS = ["https://example.com/hook1","https://example.com/hook2"];


/////////////////////////////////////////////////////////////////////


function handle_push($data){
	$repo = $data["project"]["name"];
	$branch = end(explode("/",$data["ref"]));
	$n = $data["total_commits_count"];
	$user = $data["user_username"];

	if($n > 0){
		
		//echo("repo: $repo. branch: $branch. n: $n. ");
		
		$title = "[$repo:$branch] $n new commit";
		if($n != 1) $title .= "s";
		
		//echo("title: \"$title\"\n");
		
		$embed = array(
			"title" => $title,
			"description" => ""
		);
		
		foreach($data["commits"] as $commit){
			$hash7   = substr($commit["id"],0,7);
			$url     = $commit["url"];
			$message = trim($commit["message"]);
			
			$line = "[`$hash7`]($url) $message - $user\n";
			
			$embed["description"] .= $line;
		}
		
		
		
		$payload = array(
			"content" => "",
			"url" => $data["project"]["web_url"],
			"embeds" => array($embed)
		);
		
		return $payload;
	} else {
		return null;
	}
}


/////////////////////////////////////////////////////////////////////

$body = file_get_contents("php://input");
$data = json_decode($body, true);
$event = apache_request_headers()["X-Gitlab-Event"];

switch($event){
	case "Push Hook":
		$payload = handle_push($data);
}

if($payload != null){
	$payload["username"] = "GitLab";
	$payload["avatar_url"] = "https://about.gitlab.com/images/press/logo/png/gitlab-icon-rgb.png";

	foreach($HOOKS as $hook){
		$c = curl_init($hook);
		curl_setopt($c, CURLOPT_POSTFIELDS, json_encode($payload));
		curl_exec($c);
		curl_close($c);
	}
}
?>
